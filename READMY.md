### Instalacja
- klonujemy projekt do dowolnego pustego katalogu
- przechodzimy do katalogu modal-viewer
- następnie uruchamiamy komendę yarn 

### Uruchamianie
- aby uruchomić aplikację należy wywołać komendę yarn dev
- aplikacja będzie uruchomiona pod portem localhost:3000

### Uwaga
Z powodu że obrazki z jsonplaceholder.typicode są strasznie monotonne dodałem specjalny parametr w pliku .env który pozwala na zmienienie domyśnych obrazków na randomowe.
Ta opcja jest domyślnie ustawiona na true co powoduje ze projekt o wiele lepie prezentuje się.
