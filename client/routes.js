const routes = require('next-routes')

module.exports = routes()
  .add('AlbumList', '/')
  .add('AlbumDetails', '/details/:albumId')
  .add('AuthorDetails', '/author/:authorId')
