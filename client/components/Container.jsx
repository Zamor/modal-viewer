import styled from 'styled-components'

const Container = styled.div`
  max-width: 1335px;
  margin: 0 auto;
  width: 100%;
  box-sizing: border-box;
  padding: 20px;
`
export default Container
