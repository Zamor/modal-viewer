import React from 'react'
import styled from 'styled-components'

import Container from './Container'
import { Link } from '../routes'

const Header = () => (
  <HeaderWrapper>
    <MenuContent>
      <Link route='/'>
        <HeaderTitle>
          Albumeria
        </HeaderTitle>
      </Link>
      <UserActions>
        <FakeButton>Register</FakeButton>
        <FakeButton>Login</FakeButton>
      </UserActions>
    </MenuContent>
  </HeaderWrapper>
)

const HeaderWrapper = styled.div`
  background-color: ${props => props.theme.colors.white};
  box-shadow: 0px 0px 9px 1px rgba(4, 4, 4, 0.1);
  width: 100%;
  box-sizing: border-box;
  z-index: 2;
  position: sticky;
  top: 0px;
`
const MenuContent = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const HeaderTitle = styled.div`
  font-size: 18px;
  color: #ff4865;
  font-weight: 600;
  letter-spacing: 1px;

  :hover {
    cursor: pointer;
  }
`
const UserActions = styled.div`
  > :last-child {
    margin-left : 10px;
  }
`

const FakeButton = styled.button`
  background-color: #ff4865;
  width: 100px;
  text-align: center;
  line-height: 30px;
  font-size: 15px;
  border: 0;
  border-radius: 20px;
  color: ${props => props.theme.colors.white};



`

export default Header
