import React, { PureComponent } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

class Gallery extends PureComponent {
    state = {
      currentImageIndex: this.props.initialIndex || 0
    }

  changeSelectedImageIndex = isIncrease => () => {
    const { imagesArray } = this.props
    let { currentImageIndex } = this.state

    let newIndex = isIncrease ? ++currentImageIndex : --currentImageIndex
    console.log(newIndex)

    if (newIndex === imagesArray.length) {
      newIndex = 0
    } else if (newIndex < 0) {
      newIndex = imagesArray.length - 1
    }

    this.setState({ currentImageIndex: newIndex })
  }

  getCurrentIndexImageURL = () => {
    const { imagesArray } = this.props
    const { currentImageIndex } = this.state

    const image = imagesArray[currentImageIndex] || {}

    return process.env.CUSTOM_IMAGES ? `https://picsum.photos/id/${image.id}/600/600` : image.thumbnailUrl
  }

  handleClose = () => this.props.closeGallery(false)

  render () {
    return (
      <Curtain>
        <CloseGallery onClick={this.handleClose}>X</CloseGallery>
        <ButtonWrapper>
          <ControlButton isDecrease onClick={this.changeSelectedImageIndex(false)} />
        </ButtonWrapper>
        <Image src={this.getCurrentIndexImageURL()} />
        <ButtonWrapper>
          <ControlButton onClick={this.changeSelectedImageIndex(true)} />
        </ButtonWrapper>
      </Curtain>
    )
  }
}

const Curtain = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0,0,0,0.8);
  position: fixed;
  top: 0;
  z-index: 3;
  justify-content: center;
  align-items: center;
`
const ButtonWrapper = styled.div`

`
const ControlButton = styled.div`
  border: ${props => `solid ${props.theme.colors.white}`};
  border-width: 0 3px 3px 0;
  display: flex;
  padding: 3px;
  -webkit-transform: rotate(-45deg); 
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);

  ${props => props.isDecrease && `
    -webkit-transform: rotate(135deg); 
    -ms-transform: rotate(135deg);
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
  `};
  height: 25px;
  width: 25px;
  background-color: rgba(0,0,0,0);
  cursor: pointer;
`
const Image = styled.img`
  display: flex;
  margin: 0 5px;
  max-width: 100%;
  max-height: 100%;
  width: auto;
  height: auto;
  overflow: hidden;
`

const CloseGallery = styled.div`
  font-size: 25px;
  position: fixed;
  top: 15px;
  right: 15px;
  z-index: 5;
  color: ${props => `${props.theme.colors.white}`};
  cursor: pointer;
`

Gallery.propTypes = {
  closeGallery: PropTypes.func,
  imagesArray: PropTypes.array,
  initialIndex: PropTypes.number
}

export default Gallery
