import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

class Title extends PureComponent {
  render () {
    return (
      <TitleWrapper>
        {this.props.title}
      </TitleWrapper>
    )
  }
}

const TitleWrapper = styled.h1`
  font-weight: 600;
  line-height: 1.2;
  color: #ff4865;
  font-size: 18px;
  position: relative;
  padding-bottom: 10px;

  &::after {
    width: 100px;
    height: 5px;
    content: "";
    position: absolute !important;
    bottom: 0;
    left: -15px;
    z-index: 0;
    background: -webkit-radial-gradient(50% 50%, ellipse closest-side, #ff4865, rgba(255, 42, 112, 0) 60%);
    background: -moz-radial-gradient(50% 50%, ellipse closest-side, #ff4865, rgba(255, 42, 112, 0) 60%);
    background: -ms-radial-gradient(50% 50%, ellipse closest-side, #ff4865, rgba(255, 42, 112, 0) 60%);
    background: -o-radial-gradient(50% 50%, ellipse closest-side, #ff4865, rgba(255, 42, 112, 0) 60%);
  }
`

Title.propTypes = {
  title: PropTypes.string
}

export default Title
