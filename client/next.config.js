// -- WEBPACK
const webpack = require('webpack')
require('dotenv').config()
// Declare next config
module.exports = {
  distDir: './.next',
  useFileSystemPublicRoutes: false,
  webpack: (config, { defaultLoaders, dev, ...options }) => {
    // Load all envs to next
    config.plugins.push(new webpack.EnvironmentPlugin(Object.keys(process.env)))
    return config
  }
}
