import AppStore from '../stores/AppStore'

let store = null

const storeInit = (initState = {}) => {
  // if its on server then create a new instance
  if (typeof window === 'undefined') {
    return new AppStore(initState)
  }

  if (!store) {
    store = new AppStore(initState)
  }

  return store
}

export default storeInit
