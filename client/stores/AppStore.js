import { observable, transaction, action, computed, set } from 'mobx'

import api from '../helpers/api'
import { parseObjectToQueryString } from '../helpers/queryStringParser'

class AppStore {
  constructor (initialState) {
    set(this, { ...initialState })
  }

  @observable _loading = false
  @observable _albums = {}
  @observable _authors = {}
  @observable _photos = {}
  @observable _selectedAuthor = null
  @observable _currentSelectedAlbum = null
  @observable _page = 0

  @action setLoading (isLoading) {
    this._loading = isLoading
  }

  @computed get loading () {
    return this._loading
  }

  @action setAlbums (albumsArray) {
    albumsArray.forEach(singleAlbum => {
      this._albums[singleAlbum.id] = singleAlbum
    })
  }

  @computed get albums () {
    return this._albums
  }

  @computed get pageNumber () {
    return this._page
  }

  @action setAuthors (authorsArray) {
    authorsArray.forEach(newAuthor => {
      this._authors[newAuthor.id] = newAuthor
    })
  }

  @computed get authors () {
    return this._authors
  }

  @action setPhotos (photosArray, albumId) {
    this._photos[albumId] = photosArray
  }

  @computed get photos () {
    return this._photos
  }

  // fetch method
  fetchData = (dataType, queryParams) => api.get(`${dataType}?${parseObjectToQueryString(queryParams)}`)

  @action async fetchAlbums () {
    this.setLoading(true)
    // increase page number
    this._page++
    // gathering album list
    let albumList = []
    try {
      albumList = await this.fetchData('albums', { _page: this._page })
      if (!albumList.length) {
        this._page = -1 // to disable load more
      }
    } catch (e) {
      console.error(e.message)
      return
    }

    // get already knows authors ids
    const cachedAuthorsIds = Object.keys(this._authors)
    // prepare unknow authors ids
    const newAuthorsIds = [...new Set(albumList.map(singleAlbum => singleAlbum.userId))]
      .filter(authorId => !cachedAuthorsIds.includes(authorId))

    // get unknow authors
    let newAuthorList = []
    try {
      newAuthorList = await this.fetchData('users', { id: newAuthorsIds })
    } catch (e) {
      console.error(e.message)
    }

    transaction(() => {
      newAuthorList.length && this.setAuthors(newAuthorList)
      albumList.length && this.setAlbums(albumList)
      this.setLoading(false)
    })
  }

  @action async fetchPhotos (albumId, authorId) {
    const photosList = await api.get(`photos?${parseObjectToQueryString({ albumId })}`)

    const album = this.albums.find(album => album.id === albumId)
    const author = this.authors[authorId]
    transaction(() => {
      album.photos = photosList
      if (author.albums) {
        author.albums[albumId] = album
      } else {
        author.albums = { [albumId]: album }
      }
    })
  }

  @action async changeCurrentAlbum (albumId) {
    // gathering album
    let album = this.albums[albumId]
    // if album is not in collection then get it from apu
    if (!album) {
      try {
        const albumResults = await this.fetchData('albums', { id: albumId })
        album = albumResults.length ? albumResults[0] : null
        // if gathered album didnt exist then set current sellected to null
        if (!album) {
          this._currentSelectedAlbum = null
          return
        }
      } catch (e) {
        console.error(e.message)
        this._currentSelectedAlbum = null
        return
      }
    }

    const { userId } = album

    // check for author in collection
    if (!this.authors[userId]) {
      try {
        const author = await this.fetchData('users', { id: userId })
        // set author to collection if exist
        author.length && this.setAuthors(author)
      } catch (e) {
        console.error(e.message)
      }
    }

    // check for photos in collection
    if (!this.photos[albumId]) {
      try {
        const photos = await this.fetchData('photos', { albumId: albumId })
        // set photos to collection if exists
        photos.length && this.setPhotos(photos, albumId)
      } catch (e) {
        console.error(e.message)
      }
    }

    this._currentSelectedAlbum = album
  }

  @computed get currentSelectedAlbum () {
    if (!this._currentSelectedAlbum) {
      return null
    }

    const { id, userId } = this._currentSelectedAlbum

    const photos = this.photos[id]
    const author = this.authors[userId]

    return {
      album: this._currentSelectedAlbum,
      photos,
      author
    }
  }

  @action async changeCurrentAuthor (authorId) {
    // gathering author
    let author = this.authors[authorId]
    if (!author) {
      try {
        const authorResults = await this.fetchData('users', { id: authorId })
        author = authorResults.length && authorResults[0]
      } catch (e) {
        console.error(e.message)
      }
    }

    if (!author) {
      this._selectedAuthor = null
      return
    }

    // gathering aluthor albums
    if (!author.albums) {
      try {
        const authorAlbums = await this.fetchData('albums', { userId: authorId })
        if (authorAlbums.length) {
          author.albums = authorAlbums
          this.setAuthors([author])
        }
      } catch (e) {
        console.error(e.message)
      }
    }

    this._selectedAuthor = author
  }

  @computed get currentAuthor () {
    return this._selectedAuthor
  }
}

export default AppStore
