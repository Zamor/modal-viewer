const themeConfig = {
  colors: {
    black: '#202020',
    grey: '#777777',
    orange: '#ff4865',

    blue: '#1976d2',
    white: '#fff'
  }
}

export default themeConfig
