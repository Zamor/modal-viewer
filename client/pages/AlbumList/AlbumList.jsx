import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'

import Container from '../../components/Container'
import ListItem from '../../components/ListItem'
import Title from '../../components/Title'
@inject('store')
@observer
class AlbumList extends Component {
  static async getInitialProps ({ store }) {
    const { _page } = store

    if (!_page) {
      await store.fetchAlbums()
    }

    return { }
  }

  renderAlbumList = () => {
    const { store: { authors, albums } } = this.props
    const albumsArray = albums && Object.values(albums)

    if (!albumsArray || !albumsArray.length) {
      return null
    }

    return albumsArray.map(singleAlbum => {
      const { id, title } = singleAlbum
      const authorName = authors[singleAlbum.userId] && authors[singleAlbum.userId].name

      return (
        <ListItem
          key={`album-${id}`}
          title={title}
          author={authorName}
          albumId={id}
        />
      )
    })
  }

  handleLoad = () => this.props.store.fetchAlbums()

  render () {
    const { store: { loading, pageNumber } } = this.props
    const albumList = this.renderAlbumList()

    return (
      <AlbumContainer>
        <Title title='Available albums' />
        <AlbumListWrapper>
          {albumList || 'Brak albumów do wyświetlenia'}
        </AlbumListWrapper>
        {loading && 'wczytywanie'}
        {albumList && pageNumber > 0 && <LoadMoreButton onClick={this.handleLoad}>Load More</LoadMoreButton>}
      </AlbumContainer>
    )
  }
}

const AlbumContainer = styled(Container)`
  margin-top: 30px;
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
`
const AlbumListWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
`
const LoadMoreButton = styled.button`
  border: 1px solid ${props => props.theme.colors.orange};
  color: ${props => props.theme.colors.orange};
  padding: 10px 50px;
  background-color: ${props => props.theme.colors.white};
  border-radius: 20px;
  align-self: center;
  margin-top: 15px;

  :hover {
    cursor: pointer;
  }
`

AlbumList.propTypes = {
  store: PropTypes.shape({
    loading: PropTypes.bool,
    albums: PropTypes.objectOf(PropTypes.shape({
      title: PropTypes.string,
      id: PropTypes.number
    })),
    authors: PropTypes.objectOf(PropTypes.shape({
      name: PropTypes.string
    })),
    fetchAlbums: PropTypes.func,
    pageNumber: PropTypes.number
  })
}

export default AlbumList
