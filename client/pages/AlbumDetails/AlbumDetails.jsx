import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'

import Container from '../../components/Container'
import Thumb from '../../components/Thumbnail'
import Gallery from '../../components/Gallery'

import { Link } from '../../routes'

@inject('store')
@observer
class AlbumDetails extends Component {
  static async getInitialProps ({ query, store }) {
    const { albumId } = query
    await store.changeCurrentAlbum(albumId)
    return { }
  }

  state = {
    selectedThumbnail: false
  }

  setGalleryInitialImage = selectedThumbnail => this.setState({ selectedThumbnail })

  renderThumbnails = () => {
    const { store: { currentSelectedAlbum: { photos } } } = this.props

    if (!photos || !photos.length) {
      return 'Brak zdjęć do wyświetlenia'
    }

    return photos.map((singlePhoto, idx) => {
      const url = process.env.CUSTOM_IMAGES ? `https://picsum.photos/id/${singlePhoto.id}/150/150` : singlePhoto.thumbnailUrl
      return (
        <Thumb
          key={`thumb-${singlePhoto.id}`}
          src={url}
          selectThumb={this.setGalleryInitialImage}
          thumbIndex={idx}
        />
      )
    })
  }

  render () {
    const { store: { currentSelectedAlbum } } = this.props
    const { selectedThumbnail } = this.state

    if (!currentSelectedAlbum) {
      return <div>404</div>
    }

    return (

      <AlbumContainer>
        <AlbumTitle>
          {currentSelectedAlbum.album.title}
        </AlbumTitle>
        <AlbumAuthor>
          <Link route={`/author/${currentSelectedAlbum.author.id}`}>
            <div>
            Created by: {currentSelectedAlbum.author.name}
            </div>
          </Link>
        </AlbumAuthor>
        <Photos>
          {this.renderThumbnails()}
        </Photos>
        {selectedThumbnail !== false &&
          <Gallery initialIndex={selectedThumbnail} imagesArray={currentSelectedAlbum.photos} closeGallery={this.setGalleryInitialImage} />}
      </AlbumContainer>
    )
  }
}

const AlbumContainer = styled(Container)`
  margin-top: 30px;
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
`
const AlbumTitle = styled.h1`
  font-size: 18px;
  font-weight: 600;
  color: ${props => props.theme.colors.black};
  margin-bottom: 10px;

  &::first-letter {
    text-transform: uppercase;
  }
`
const AlbumAuthor = styled.h4`
  color: ${props => props.theme.colors.grey};
  font-size: 14px;
  margin-bottom: 15px;
  cursor: pointer;
`
const Photos = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`

AlbumDetails.propTypes = {
  store: PropTypes.shape({
    loading: PropTypes.bool,
    currentSelectedAlbum: PropTypes.shape({
      photos: PropTypes.arrayOf(PropTypes.object),
      author: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string
      }),
      album: PropTypes.shape({
        title: PropTypes.string
      })
    })
  })
}

export default AlbumDetails
